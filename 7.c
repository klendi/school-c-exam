#include <stdio.h>

int Shuma(int n, int sh)
{
   if(n == 0)
   {
       return sh;
   }
   else
   {
       sh += n % 10;
       return Shuma(n / 10, sh);
   }
}

int main()
{
   int n = 153;
   int shuma = Shuma(n,0);
   printf("%d", shuma);
}
