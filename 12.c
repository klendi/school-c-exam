//
// Created by klendi on 8.7.19.
//

#include <stdio.h>
#include <stdlib.h>

int MatriceCount(size_t n, int V[n][n], int x)
{
   int i,j, count;
   for(i = 0; i < n; i++)
   {
       for(j = n - i; j < n; j++)
       {
           if(V[i][j] % x == 0)
               count++;
       }
   }

   return count;
}

int main()
{
   int M[4][4] = {
           {1, 2, 3, 4},
           {5, 6, 7, 8},
           {9, 10, 11, 12},
           {13, 14, 15, 16}
   };
   MatriceCount(4, M, 3);
}
