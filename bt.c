//
// Created by klendi on 8.7.19.
//

#include <stdio.h>
#include <stdlib.h>

struct Node
{
   int data;
   struct Node * right;
   struct Node * left;
};

struct Node * newNode(int data)
{
   struct Node * root = (struct Node*)malloc(sizeof(struct Node));
   root->right = NULL;
   root->left = NULL;
   root->data = data;

   return root;
}

struct Node * Mirror(struct Node * root)
{
   if(root == NULL) return root;
   else
   {
       Mirror(root->left);
       Mirror(root->right);

       struct Node * temp;
       temp = root->left;
       root->left = root->right;
       root->right = temp;
   }

   return root;
}

void inOrder(struct Node* node)
{
   if (node == NULL)
       return;

   inOrder(node->left);
   printf("%d ", node->data);
   inOrder(node->right);
}

int Shuma(struct Node * root)
{
   if(root == NULL) return 0;
   else
       return root->data + Shuma(root->left) + Shuma(root->right);
}

int main()
{
   struct Node * root = newNode(1);
   root->left = newNode(2);
   root->right = newNode(3);
   root->left->left = newNode(4);
   root->left->right = newNode(5);
   root->right->left = newNode(6);
   root->right->right = newNode(7);
   root->right->left->right = newNode(8);

   inOrder(root);
   puts("\n");
   struct Node * mirroredRoot = Mirror(root);
   inOrder(mirroredRoot);


}
