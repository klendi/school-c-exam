//
// Created by klendi on 8.7.19.
//
#include <stdio.h>
#include <stdlib.h>

int * RecursiveFunc(size_t n, int T[n])
{
   if(n == -1)
   {
       return T;
   }
   else if(n >= 0 && T[n - 1] < 0)
   {
       T[n - 1] = 0;
       return RecursiveFunc(n - 1, T);
   }
   else
       return RecursiveFunc(n - 1, T);

}

int main()
{
   int T[5] = {1, -2, 3, -1, 8};
   int i = 0;
   int n = 5;

   int * arr = RecursiveFunc(n, T);

   for(i = 0; i < n; i++)
       printf("%d\n", arr[i]);

   return 0;
}
