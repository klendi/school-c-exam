//
// Created by klendi on 8.7.19.
//
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct Node
{
   int key;
   struct Node *left, *right;
};

struct Node* NewNode(int key)
{
   struct Node* node = (struct Node*)malloc(sizeof(struct Node));
   node->key = key;
   node->left = NULL;
   node->right = NULL;
   return node;
}

int SumBinaryTree(struct Node* root)
{
   if(root == NULL) return 0;

   return (root->key + SumBinaryTree(root->left) + SumBinaryTree(root->right));
}

struct Node* Search(struct Node* root, int key)
{
   if(root == NULL || root->key == key)
       return root;

   if(root->key < key)
       return Search(root->right, key);

   return Search(root->left, key);
}

struct Node* Insert(struct Node* root, int key)
{
   if(root == NULL) return NewNode(key);

   if(key < root->key)
       return Insert(root->left, key);
   else if(key > root->key)
       return Insert(root->right, key);

   return root;
}

struct Node* Mirror(struct Node* node)
{
   if(node == NULL)
       return NULL;
   else
   {
       struct Node* temp;
       Mirror(node->left);
       Mirror(node->right);

       temp = node->left;
       node->left = node->right;
       node->right = temp;
   }
}
void inOrder(struct Node* node)
{
   if (node == NULL)
       return;

   inOrder(node->left);
   printf("%d ", node->key);
   inOrder(node->right);
}

int SumOfPositiveNodes(struct Node* node)
{
   if(node == NULL)
       return 0;

   int shuma = 0;

   if(node->key > 0)
       shuma += node->key;

   if(node->left != NULL)
   {
       shuma += SumOfPositiveNodes(node->left);
   }
   if(node->right != NULL)
   {
       shuma += SumOfPositiveNodes(node->right);
   }

   return shuma;
}

bool isLeaf(struct Node* node)
{
   return node->right == NULL && node->left == NULL;
}

int heigh(struct Node* node)
{
   if(node == NULL)
       return 0;

   int leftMaxDepth = heigh(node->left);
   int rightMaxDepth = heigh(node->right);

   if (leftMaxDepth > rightMaxDepth)
       return (leftMaxDepth + 1);

   else return (rightMaxDepth + 1);

}

bool leavesHaveSomeHeigh(struct Node* node)
{
   int lHeight = heigh(node->left);
   int rHeight = heigh(node->right);

   return  lHeight == rHeight;
}

int main()
{
   struct Node *root = NewNode(12);

   Insert(root, 5);
   Insert(root, 3);
   Insert(root, 7);
   Insert(root, 1);
   bool status = leavesHaveSomeHeigh(root);

   if(status)
       printf("YES");
   else
       printf("No");
}
