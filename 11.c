//
// Created by klendi on 8.7.19.
//
#include <stdio.h>

int * Func(size_t n, int v[n])
{
   int i = 0;
   for(i = 0; i < n; i++)
   {
       if(v[i] > 0)
           v[i] = 1;
       else
           v[i] = 0;
   }

   return v;
}

int main()
{
   int a[5] = {1, 5, -5, -2, 3};
   int *arr = Func(5, a);

   int i = 0;
   for(i = 0; i < 5; i++)
   {
       printf("Nr eshte: %d\n", arr[i]);
   }

}
