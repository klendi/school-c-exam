////
//// Created by klendi on 6.7.19.
////
////Merr vektorin me nr N te plote, zevendeson elementet e tij me 1 nqs pozitiv me 0 nqs negativ
//#include "stdio.h"
//
int * Func(int vek[], int n)
{
   int i = 0;
   for(i = 0; i < n; i++)
   {
       if(vek[i] > 0)
           vek[i] = 1;
       else
           vek[i] = 0;
   }

   return vek;
}

int main()
{
   int v[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, -1, -3, -3, 4, -2, -10};
   int *ret = Func(v, 15);
   int i = 0;
   for(i = 0; i < 15; i++)
   {
       printf("%d", ret[i]);
   }

}
