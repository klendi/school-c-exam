//
// Created by klendi on 7.7.19.
//

#include <stdio.h>

int * FuncRec(int v[], int n)
{
   if(n < 0)
       return v;
   else
   {
       if(v[n] < 0)
       {
           v[n] = 0;
       }
       return FuncRec(v, n - 1);
   }
}

int main()
{
   int a[5] = {1,10,32,-12, 5};
   int *v = FuncRec(a, 5);
   int i = 0;

   for(i = 0; i < 5; i++)
   {
       printf("el: %d\n", v[i]);
   }

   return 0;
}
