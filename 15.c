//
// Created by klendi on 8.7.19.
//

#include <stdio.h>
#include <stdlib.h>

int main()
{
   int m;
   scanf("%d", &m);

   //afishon pjesen e siperme te rombit
   //kapim cdo rresht nje e nga nje
   for(int i=1;i<=m;i++)
   {
       //afisho hapesira
       for(int j=1;j<=m-i;j++)
       {
           printf(" ");
       }
       //afisho yje
       for(int j=1;j<=2*i-1;j++)
       {
           printf("*");
       }
       //kalo rresht te ri
       printf("\n");
   }

   //afishon pjesen e poshtme te rombit
   //kapim cdo rresht nje e nga nje
   for(int i=1;i<=m-1;i++)
   {
       //afisho hapesira
       for(int j=1;j<=i;j++)
       {
           printf(" ");
       }
       //afisho yje
       for(int j = 1; j <= 2 * m - 2 * i - 1;j++)
       {
           printf("*");
       }
       //kalo rresht te ri
       printf("\n");
   }
}
