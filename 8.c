//
// Created by klendi on 7.7.19.
//

#include <stdio.h>
#include <stdlib.h>

struct Node
{
   int data;
   struct Node* nextPtr;
};

struct Node* NewNode()
{
   struct Node* node = (struct Node*)malloc(sizeof(struct Node));
   node->nextPtr = NULL;
   return node;
}

void ListTraversal(struct Node* list)
{
   while (list != NULL)
   {
       printf("%d\n", list->data);
       list = list->nextPtr;
   }
}

void Insert(struct Node** root, int data)
{
   struct Node* newNode = (struct Node*)malloc(sizeof(struct Node));
   newNode->data = data;
   newNode->nextPtr = (*root);
   (*root) = newNode;
}

void InsertAtStart(struct Node** root, int data)
{
   struct Node* node = (struct Node*)malloc(sizeof(struct Node));
   node->data = data;
   node->nextPtr = (*root);
   (*root) = node;
}

void Append(struct Node** root, int data)
{
   struct Node* newNode = (struct Node*)malloc(sizeof(struct Node));
   struct Node* last = *root;
   newNode->data = data;
   newNode->nextPtr = NULL;

   //If its empty make this the only element and the root
   if(*root == NULL)
   {
       *root = newNode;
       return;
   }
   //Go to the last node and then to the last node we link it with our node
   while(last->nextPtr != NULL)
   {
       last = last->nextPtr;
   }

   last->nextPtr = newNode;
}

void InsertAfterNode(struct Node* previousNode, int data)
{
   if(previousNode == NULL)
   {
       return;
   }
   struct Node* newNode = (struct Node*)malloc(sizeof(struct Node));
   newNode->data = data;
   newNode->nextPtr = previousNode->nextPtr;
   previousNode->nextPtr = newNode;
}

int main()
{
   struct Node* head = NewNode();
   Insert(&head, 7);
   Insert(&head, 15);
   InsertAfterNode(head->nextPtr, 44);
   Append(&head, 55);
   ListTraversal(head);
}

void Append1(struct Node** root, int data)
{
   struct Node* newNode = NewNode();
   struct Node* last = *root;

   newNode->data = data;

   while (last->nextPtr != NULL)
       last = last->nextPtr;

   last->nextPtr = newNode;

}
