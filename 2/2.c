//
// Created by klendi on 6.7.19.
//

#include "stdio.h"
#include "stdlib.h"
#include "string.h"

int main()
{
    FILE * fileNota = fopen("/home/klendi/Desktop/Projects/School/2/nota.txt", "r");
    FILE * fileStudent = fopen("/home/klendi/Desktop/Projects/School/2/student.txt", "r");
    FILE * fileKursi = fopen("/home/klendi/Desktop/Projects/School/2/kursi.txt", "r");

    //dimensionet e matrices
    int n, m;

    fscanf(fileNota, "%d %d", &m, &n);
    int matrix[m][n];
    char kurset[n][128];
    int i, j = 0, count = 0;

    char line[256];
    char line2[256];

    while (fgets(line, sizeof line, fileNota ) != NULL )
    {
        if(count == 0)
        {
            count++;
        }
        else
        {
            sscanf(line, "%d %d %d %d", &matrix[j][0], &matrix[j][1], &matrix[j][2], &matrix[j][3]);
            j++;
        }
    }
    j = 0;
    while (fgets(line2, sizeof line2, fileKursi ) != NULL )
    {
        sscanf(line2, "%[^\\t\\n]s", kurset[j]);
        j++;
    }
    char c;
    while ((c = fgetc(fileKursi)) != EOF)
    {

    }
    double mesKurseve[n];
    double max;
    int maxIndex;

    for(i = 0; i < n; i++)
    {
        for(j = 0; j < m; j++)
        {
            mesKurseve[i] += matrix[j][i];
        }

        mesKurseve[i] = mesKurseve[i] / 4;

        if(i == 0)
            max = mesKurseve[i];

        if(mesKurseve[i] > max)
        {
            max = mesKurseve[i];
            maxIndex = i;
        }
    }

    printf("Kursi i %d : %s ka mestararen me te madhe", maxIndex + 1, kurset[maxIndex]);


}