//
// Created by klendi on 7.7.19.
//

//S = (1 / 4) - (3 / 5) - (2 / 8) + ( 6 / 10 ) - (4 / 16) - (12 / 20)
//S = (1 / 4) - [(b - a) / (a + b)]

#include <stdio.h>

double ShumeSerise(int n)
{
   int i;
   double shuma = 1 / 4;
   int a = 1;
   int b = 4;

   for(i = 2; i < n + 1; i++)
   {
       if(i % 3 == 0)
       {
           shuma += (b - a) / (a + b);
       }
       else
       {
           shuma -= (b - a) / (a + b);
       }

       a = b - a;
       b = a + b;
   }

   printf("Shuma eshte %.2lf", shuma);
}

int main()
{
   ShumeSerise(4);
}
