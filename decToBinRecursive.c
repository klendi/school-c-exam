#include <stdio.h>

int find(int decimal_number);
int findBase4(int decimal);

int main() {
   printf("Ju lutem vendosni numrin e plote!\n");
   int n;
   scanf("%d", &n);

   int bin = find(n);
   printf("%d", bin);
   return 0;
}

int find(int decimal_number)
{
   if (decimal_number == 0)
       return 0;
   else
       return (decimal_number % 2 + 10 * find(decimal_number / 2));
}

int findBase4(int decimal)
{
   if(decimal == 0)
       return 0;
   else
       return (decimal % 4 + (10 * findBase4(decimal / 4)));
}
