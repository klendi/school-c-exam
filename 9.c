#include <stdio.h>

void Func(size_t size, int v[size])
{
   int freq[size];
   int newArr[2][size];
   int  i, j, count;


   for(i=0; i<size; i++)
   {
       freq[i] = -1;
   }


   for(i=0; i<size; i++)
   {
       count = 1;
       for(j=i+1; j<size; j++)
       {
           /* Nqs kemi gjetur nje numer te njejte, duplikant */
           if(v[i]==v[j])
           {
               count++;

               /* E bejme zero sepse nuk duam ta rrisim frekuencen e ketij prap */
               freq[j] = 0;
           }
       }

       /* Nqs frekuencia nuk eshte llogaritur */
       if(freq[i] != 0)
       {
           freq[i] = count;
       }
   }
   count = 0;
   for(i=0; i<size; i++)
   {
       if(freq[i] != 0)
       {
           newArr[0][count] = v[i];
           newArr[1][count] = freq[i];
           printf("Nr: %d, frekuenca: %d here\n", newArr[0][count], newArr[1][count]);
           count++;
       }
   }
}

int main()
{
   int v[5] = {1, 2, 2, 5, 7};
   Func(5, v);
}
